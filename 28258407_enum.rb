module P2Enumerable
  def blank? (input)
    if input == nil || input == false
      return true
    end
    return false
  end

    def p2all?
    p2each do |e|
      blank?(yield(e)) ? (return false) : true
    end
    return true
  end
def p2any?
    p2each do |e|
      blank?(yield(e)) ? true : (return true)
    end
    return false
  end


  def p2collect
    r = Array.new
    p2each do |e|
      r << yield( e )
    end
    return r
  end

  def p2collect_concat
    r = Array.new
    p2each do |e|
      r << e
      res = yield( e )
      r << res
    end
    return r
  end


  def p2count( filter = nil )
    count = 0
    if filter != nil
      p2each do |e|
        if block_given?
          if yield(e) == filter
            count += 1
          end
        else
          if e == filter
            count += 1
          end
        end
      end
    else
      p2each do |e|
        if block_given?
          if yield(e)
            count += 1
          end
        else
          count += 1
        end
      end
    end
    return count
  end


  def p2cycle ( n=nil, &block )
    r = n
    if r==nil
      p2each do |e|
        yield(e)
      end
      p2cycle(nil)
    end
    if r<1
      return
    end
    p2each do |e|
      yield(e)
    end
    p2cycle(r-1){block}
  end

  def p2detect ( ifnone = nil )
    p2each do |e|
      if yield(e) != false
        return e
      end
    end
    if ifnone != nil
      return ifnone
    end
    return nil
  end


  def p2drop( n )
    count = n
    ret = Array.new
    p2each do |e|
      if count > 0
        count -=1
      else
        ret << e
      end
    end
    return ret
  end


  def p2drop_while
    triggered = false
    ret = Array.new
    p2each do |e|
      if triggered
        ret << e
      elsif blank?(yield(e))
        triggered = true
        ret << e
      end
    end
    return ret
  end


  def p2each_cons( n )
  end

  def p2each_slice( n )
    count = n
    temp = Array.new
    p2each do |e|
      if count > 0
        count -= 1
        temp << e
      else
        count = n-1
        yield(temp)
        temp = Array.new
        temp << e
      end
    end
    yield(temp)
  end

  def p2each_with_index
    count = 0
    p2each do |e|
      yield(e, count)
      count+=1
    end
  end


  def p2entries( *args )
    ret = Array.new
    p2each do |e|
      ret << e
    end
    return ret
  end


  alias p2find p2detect


  def p2find_all
    ret = Array.new
    p2each do |e|
      if yield(e)
        ret << e
      end
    end
    return ret
  end



  def p2find_index
    count = -1
    p2each do |e|
      count+=1
      if yield(e)
        return count
      end
    end
    return nil
  end



  def p2first( n )
    count = n
    ret = Array.new
    p2each do |e|
      if count > 0
        ret << e
        count -= 1
      end
    end
    return ret
  end


  def p2group_by
    ret = Hash.new {|hash,key| hash[key] = Array.new }
    p2each do |e|
      ret[yield(e)] << e
    end
    return ret
  end


  def p2inject( init )
    r = init
    p2each do | e |
      r = yield( r, e )
    end
    return r
  end

  def p2para0inject
  end


  def p2minmax
    min = 1 / 0.0 #infinity
    max = -1 / 0.0 #-infinity
    maxindex = 0
    minindex = 0
    count = 0
    p2each do |e|
      temp = yield(e)
      if temp > max
        max = temp
        maxindex = count
      end
      if temp < min
        min = temp
        minindex = count
      end
      count += 1
    end
    return [self[minindex], self[maxindex]]
  end


  def p2minmax_by
    min = 1 / 0.0 #infinity
    max = -1 / 0.0 #-infinity
    count = 0
    p2each do |e|
      temp = yield(e)
      if temp > max
        max = temp
      end
      if temp < min
        min = temp
      end
      count += 1
    end
    return [min, max]
  end


  def p2partition
    truea = Array.new
    falsea = Array.new
    p2each do |e|
      if yield(e)
        truea << e
      else
        falsea << e
      end
    end
    return [truea, falsea]
  end


  def p2reject
    ret = Array.new
    p2each do |e|
      if !(yield(e))
        ret << e
      end
    end
    return ret
  end


  def p2take( n )
    count = n
    ret = Array.new
    p2each do |e|
      if count > 0
        ret << e
      end
      count -= 1
    end
    return ret
  end


  def p2take_while
    triggered = false
    ret = Array.new
    p2each do |e|
      if blank?(yield(e))
        triggered = true
      elsif !triggered
        ret << e
      end
    end
    return ret
  end


  alias p2to_a p2entries


  def p2to_h
    ret = Hash.new
    p2each do | k,v |
      ret[k] = v
    end
    return ret
  end

end