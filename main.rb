#TESTS----------------------------------------------------------

def test_p2all?
  r = [1,2,3].p2all?{ |e| e+1 }
  raise "#{__method__} error" if !r
  r = ['word'].p2all?{ |e| e.length > 5 }
  raise "#{__method__} error" if r
  p "#{__method__} passed"
end

def test_p2any?
  r = [nil, 1, 2].p2any?{ |e| e }
  raise "#{__method__} error" if !r
  r = [nil, nil].p2any?{ |e| e }
  raise "#{__method__} error" if r
  p "#{__method__} passed"
end

def test_p2collect
  r = [1,2,3].p2collect{ |e| e+1 }
  raise "#{__method__} error" if r != [2,3,4]
  r = [1,2].p2collect{ 'test' }
  raise "#{__method__} error" if r != ['test', 'test']
  p "#{__method__} passed"
end

def test_p2collect_concat
  r = [1,2,3].p2collect_concat{ |e| e*e }
  raise "#{__method__} error" if r != [1,1,2,4,3,9]
  r = [1,2].p2collect_concat{ 'test' }
  raise "#{__method__} error" if r != [1, 'test', 2, 'test']
  p "#{__method__} passed"
end

def test_p2count
  r = [1,2,2].p2count{ |e| e == 2 }
  raise "#{__method__} error" if r != 2
  r = [1,2,2].p2count(2)
  raise "#{__method__} error" if r != 2
  r = [1,2,2].p2count
  raise "#{__method__} error" if r != 3
  p "#{__method__} passed"
end

def test_p2cycle
  r = [1,2,3].p2cycle(2){ |e| e+1 }
  raise "#{__method__} error" if r != [1,2,3,2,3,4]
  p "#{__method__} passed"
end

def test_p2detect
  r = [1,2,3].p2detect{ |e| e==2 }
  raise "#{__method__} error" if r != 2
  r = [1,2,3].p2detect(5+5){ |e| e==4 }
  raise "#{__method__} error" if r != 10
  r = [1,2,3].p2detect(nil){ |e| e==4 }
  raise "#{__method__} error" if r != nil
  p "#{__method__} passed"
end

def test_p2drop
  r = [1,2,3].p2drop(0)
  raise "#{__method__} error" if r != [1,2,3]
  r = [1,2,3].p2drop(2)
  raise "#{__method__} error" if r != [3]
  p "#{__method__} passed"
end

def test_p2drop_while
  r = [1,2,3].p2drop_while { |e| e<3 }
  raise "#{__method__} error" if r != [3]
  r = [1,2,3,2,1].p2drop_while { |e| e<2 }
  raise "#{__method__} error" if r != [2,3,2,1]
  p "#{__method__} passed"
end

def test_p2each
  s = 0
  [1,2,3].p2each{ |e| s += e }
  raise "#{__method__} error" if s != 6
  s = 0
  {"one"=>1,"two"=>2,"three"=>3}.p2each{ |f,e| s += e }
  raise "#{__method__} error" if s != 6
  p "#{__method__} passed"
end

def test_p2each_slice
  r = Array.new
  [1,2,3,4,5,6,7,8,9,10,11].p2each_slice(3) { |e| r << e }
  raise "#{__method__} error" if r != [[1,2,3],[4,5,6],[7,8,9],[10,11]]
  p "#{__method__} passed"
end

def test_p2each_with_index
  s = Array.new
  r = Array.new
  [2,3,4].p2each_with_index{ |e,i| s << e; r << i }
  raise "#{__method__} error" if (s != [2,3,4] || r != [0,1,2])
  p "#{__method__} passed"
end

def test_p2entries
  r = [1,2,3].p2entries
  raise "#{__method__} error" if r != [1,2,3]
  p "#{__method__} passed"
end

def test_p2find
  r = [1,2,3].p2find{ |e| e==2 }
  raise "#{__method__} error" if r != 2
  r = [1,2,3].p2find(5+5){ |e| e==4 }
  raise "#{__method__} error" if r != 10
  r = [1,2,3].p2find(nil){ |e| e==4 }
  raise "#{__method__} error" if r != nil
  p "#{__method__} passed"
end

def test_p2find_all
  r = [1,2,3].p2find_all { |e| e==2 }
  raise "#{__method__} error" if r != [2]
  r = [1,2,3,4,5,6].p2find_all { |e| e%2 == 0 }
  raise "#{__method__} error" if r != [2,4,6]
  r = [1,2,3].p2find_all { |e| e==4 }
  raise "#{__method__} error" if r != []
  p "#{__method__} passed"
end

def test_p2find_index
  r = [1,2,3].p2find_index { |e| e==2 }
  raise "#{__method__} error" if r != 1
  r = [1,2,3,4,5,6].p2find_index { |e| e == 7 }
  raise "#{__method__} error" if r != nil
  p "#{__method__} passed"
end

def test_p2first
  r = [1,2,3,4].p2first(2)
  raise "#{__method__} error" if r != [1,2]
  r = [1,2,3,4].p2first(5)
  raise "#{__method__} error" if r != [1,2,3,4]
  p "#{__method__} passed"
end

def test_p2group_by
  r = [1,2,3,4].p2group_by { |e| e%2 }
  raise "#{__method__} error" if r != {0=>[2,4], 1=>[1,3]}
  r = [1,2,3,4].p2group_by { |e| e }
  raise "#{__method__} error" if r != {1=>[1], 2=>[2], 3=>[3], 4=>[4]}
  p "#{__method__} passed"
end

def test_p2inject
  r = [1,2,3].p2inject(0){ |s, e| s + e }
  raise "#{__method__} error" if r != 6
  p "#{__method__} passed"
end

def test_p2minmax
  r = [1,2,3].p2minmax { |e| e+1 }
  raise "#{__method__} error" if r != [1,3]
  r = ['1','11','111'].p2minmax { |e| e.length }
  raise "#{__method__} error" if r != ['1','111']
  p "#{__method__} passed"
end

def test_p2minmax_by
  r = [1,2,3].p2minmax_by { |e| e+1 }
  raise "#{__method__} error" if r != [2,4]
  r = ['1','11','111'].p2minmax_by { |e| e.length }
  raise "#{__method__} error" if r != [1,3]
  p "#{__method__} passed"
end

def test_p2partition
  r = [1,2,3,4].p2partition { |e| e>2 }
  raise "#{__method__} error" if r != [[3,4],[1,2]]
  r = [1,2,3,4].p2partition { |e| e%2==0 }
  raise "#{__method__} error" if r != [[2,4],[1,3]]
  p "#{__method__} passed"
end

def test_p2reject
  r = [1,2,2].p2reject{ |e| e == 2 }
  raise "#{__method__} error" if r != [1]
  p "#{__method__} passed"
end

def test_p2take
  r = [1,2,3,4].p2take(2)
  raise "#{__method__} error" if r != [1,2]
  r = [1,2,3,4].p2take(5)
  raise "#{__method__} error" if r != [1,2,3,4]
  p "#{__method__} passed"
end

def test_p2take_while
  r = [1,2,3,4].p2take_while { |e| e < 3 }
  raise "#{__method__} error" if r != [1,2]
  r = [1,2,3,4].p2take_while { |e| e<0 }
  raise "#{__method__} error" if r != []
  r = [1,2,3,4].p2take_while { |e| e<5 }
  raise "#{__method__} error" if r != [1,2,3,4]
  p "#{__method__} passed"
end

def test_p2to_a
  r = (1..3).p2to_a
  raise "#{__method__} error" if r != [1,2,3]
  p "#{__method__} passed"
end

def test_p2to_h
  s = Array.new
  (1..3).p2to_a.p2each_with_index{ |e,i| s << [e, i] }
  r = s.p2to_h
  raise "#{__method__} error" if r != {1=>0, 2=>1, 3=>2}
  p "#{__method__} passed"
end

#TREE TESTS----------------------------------------------------

def test_tree_p2each
  s = P2Tree.new
  s.insert(3)
  s.insert(4)
  s.insert(5)
  s.insert(-1)
  t = 0
  s.p2each{ |e| t += e }
  raise "#{__method__} error" if t != 11
  p "#{__method__} passed"
end

def test_tree_p2each_with_level
  s = P2Tree.new
  s.insert(3)
  s.insert(4)
  s.insert(5)
  s.insert(-1)
  t = 0
  u = 0
  s.p2each_with_level{ |f, e| t += e; u += f}
  raise "#{__method__} error" if (t != 11 || u != 4)
  p "#{__method__} passed"
end

#CLASSES-------------------------------------------------------

class Array
  def p2each( s = 0, &block )
    return if s == self.size
    block.call( self[ s ] )
    p2each( s + 1, &block )
  end
end

class Range
  def p2each( s = 0, &block )
    return if s == self.size
    block.call( self[ s ] )
    p2each( s + 1, &block )
  end
end

class Hash
  def p2each( s = 0, &block )
    return if s == self.size
    block.call( self[ s ] )
    p2each( s + 1, &block )
  end
end

class P2Tree
  @node
  @left
  @right

  def initialize( n = nil )
    if n != nil
      @node = n
      @left = P2Tree.new
      @right = P2Tree.new
    end
  end

  def insert( n )
    if @node == nil
      @node = n
      @left = P2Tree.new
      @right = P2Tree.new
    elsif n < @node
      @left.insert(n)
    else
      @right.insert(n)
    end
  end

  def p2each( &block )
    if @node != nil
      yield(@node)
    end
    if @left != nil
      @left.p2each(&block)
    end
    if @right != nil
      @right.p2each(&block)
    end
  end

  def p2each_with_level( &block )
    level = 0
    if @node != nil
      yield(level, @node)
    end
    level +=1
    if @left != nil
      @left.p2each_with_level_helper( level, &block )
    end
    if @right != nil
      @right.p2each_with_level_helper( level, &block )
    end
  end

  def p2each_with_level_helper( level, &block )
    if @node != nil
      yield(level, @node)
    end
    level +=1
    if @left != nil
      @left.p2each_with_level_helper( level, &block )
    end
    if @right != nil
      @right.p2each_with_level_helper( level, &block )
    end
  end

end

#ENUMERABLE MODULE---------------------------------------------

module P2Enumerable

  def blank? (input)
    if input == nil || input == false
      return true
    end
    return false
  end

  def p2all?
    p2each do |e|
      blank?(yield(e)) ? (return false) : true
    end
    return true
  end

  def p2any?
    p2each do |e|
      blank?(yield(e)) ? true : (return true)
    end
    return false
  end

  def p2collect
    r = Array.new
    p2each do |e|
      r << yield( e )
    end
    return r
  end

  def p2collect_concat
    r = Array.new
    p2each do |e|
      r << e
      res = yield( e )
      r << res
    end
    return r
  end

  def p2count( filter = nil )
    count = 0
    if filter != nil
      p2each do |e|
        if block_given?
          if yield(e) == filter
            count += 1
          end
        else
          if e == filter
            count += 1
          end
        end
      end
    else
      p2each do |e|
        if block_given?
          if yield(e)
            count += 1
          end
        else
          count += 1
        end
      end
    end
    return count
  end

  def p2cycle ( n=nil, &block )
    r = n
    if r==nil
      p2each do |e|
        yield(e)
      end
      p2cycle(nil)
    end
    if r<1
      return
    end
    p2each do |e|
      yield(e)
    end
    p2cycle(r-1){block}
  end

  def p2detect ( ifnone = nil )
    p2each do |e|
      if yield(e) != false
        return e
      end
    end
    if ifnone != nil
      return ifnone
    end
    return nil
  end

  def p2drop( n )
    count = n
    ret = Array.new
    p2each do |e|
      if count > 0
        count -=1
      else
        ret << e
      end
    end
    return ret
  end

  def p2drop_while
    triggered = false
    ret = Array.new
    p2each do |e|
      if triggered
        ret << e
      elsif blank?(yield(e))
        triggered = true
        ret << e
      end
    end
    return ret
  end

  def p2each_cons( n )
  end

  def p2each_slice( n )
    count = n
    temp = Array.new
    p2each do |e|
      if count > 0
        count -= 1
        temp << e
      else
        count = n-1
        yield(temp)
        temp = Array.new
        temp << e
      end
    end
    yield(temp)
  end

  def p2each_with_index
    count = 0
    p2each do |e|
      yield(e, count)
      count+=1
    end
  end

  def p2entries( *args )
    ret = Array.new
    p2each do |e|
      ret << e
    end
    return ret
  end

  alias p2find p2detect

  def p2find_all
    ret = Array.new
    p2each do |e|
      if yield(e)
        ret << e
      end
    end
    return ret
  end

  def p2find_index
    count = -1
    p2each do |e|
      count+=1
      if yield(e)
        return count
      end
    end
    return nil
  end

  def p2first( n )
    count = n
    ret = Array.new
    p2each do |e|
      if count > 0
        ret << e
        count -= 1
      end
    end
    return ret
  end

  def p2group_by
    ret = Hash.new {|hash,key| hash[key] = Array.new }
    p2each do |e|
      ret[yield(e)] << e
    end
    return ret
  end

  def p2inject( init )
    r = init
    p2each do | e |
      r = yield( r, e )
    end
    return r
  end

  def p2para0inject
  end

  def p2minmax
    min = 1 / 0.0 #infinity
    max = -1 / 0.0 #-infinity
    maxindex = 0
    minindex = 0
    count = 0
    p2each do |e|
      temp = yield(e)
      if temp > max
        max = temp
        maxindex = count
      end
      if temp < min
        min = temp
        minindex = count
      end
      count += 1
    end
    return [self[minindex], self[maxindex]]
  end

  def p2minmax_by
    min = 1 / 0.0 #infinity
    max = -1 / 0.0 #-infinity
    count = 0
    p2each do |e|
      temp = yield(e)
      if temp > max
        max = temp
      end
      if temp < min
        min = temp
      end
      count += 1
    end
    return [min, max]
  end

  def p2partition
    truea = Array.new
    falsea = Array.new
    p2each do |e|
      if yield(e)
        truea << e
      else
        falsea << e
      end
    end
    return [truea, falsea]
  end

  def p2reject
    ret = Array.new
    p2each do |e|
      if !(yield(e))
        ret << e
      end
    end
    return ret
  end

  def p2take( n )
    count = n
    ret = Array.new
    p2each do |e|
      if count > 0
        ret << e
      end
      count -= 1
    end
    return ret
  end

  def p2take_while
    triggered = false
    ret = Array.new
    p2each do |e|
      if blank?(yield(e))
        triggered = true
      elsif !triggered
        ret << e
      end
    end
    return ret
  end

  alias p2to_a p2entries

  def p2to_h
    ret = Hash.new
    p2each do | k,v |
      ret[k] = v
    end
    return ret
  end
end

#RUN TESTS----------------------------------------------------

class Array
  include P2Enumerable
end

class Range
  include P2Enumerable
end

class Hash
  include P2Enumerable
end

test_p2all?

test_p2any?

test_p2collect

test_p2collect_concat

test_p2count

# test_p2cycle

test_p2detect

test_p2drop

test_p2drop_while

test_p2each

# test_p2each_cons

test_p2each_slice

test_p2each_with_index

test_p2entries

test_p2find

test_p2find_all

test_p2find_index

test_p2first

test_p2group_by

test_p2inject

# test_p2para0inject

test_p2minmax

test_p2minmax_by

test_p2partition

test_p2reject

test_p2take

test_p2take_while

test_p2to_a

test_p2to_h

test_tree_p2each

test_tree_p2each_with_level