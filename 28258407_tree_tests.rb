
#TREE TESTS----------------------------------------------------

def test_tree_p2each
  s = P2Tree.new
  s.insert(3)
  s.insert(4)
  s.insert(5)
  s.insert(-1)
  t = 0
  s.p2each{ |e| t += e }
  raise "#{__method__} error" if t != 11
  p "#{__method__} passed"
end

def test_tree_p2each_with_level
  s = P2Tree.new
  s.insert(3)
  s.insert(4)
  s.insert(5)
  s.insert(-1)
  t = 0
  u = 0
  s.p2each_with_level{ |f, e| t += e; u += f}
  raise "#{__method__} error" if (t != 11 || u != 4)
  p "#{__method__} passed"
end
