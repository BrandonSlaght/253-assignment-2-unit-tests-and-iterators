#TESTS----------------------------------------------------------

def test_p2all?
  r = [1,2,3].p2all?{ |e| e+1 }
  raise "#{__method__} error" if !r
  r = ['word'].p2all?{ |e| e.length > 5 }
  raise "#{__method__} error" if r
  p "#{__method__} passed"
end

def test_p2any?
  r = [nil, 1, 2].p2any?{ |e| e }
  raise "#{__method__} error" if !r
  r = [nil, nil].p2any?{ |e| e }
  raise "#{__method__} error" if r
  p "#{__method__} passed"
end

def test_p2collect
  r = [1,2,3].p2collect{ |e| e+1 }
  raise "#{__method__} error" if r != [2,3,4]
  r = [1,2].p2collect{ 'test' }
  raise "#{__method__} error" if r != ['test', 'test']
  p "#{__method__} passed"
end

def test_p2collect_concat
  r = [1,2,3].p2collect_concat{ |e| e*e }
  raise "#{__method__} error" if r != [1,1,2,4,3,9]
  r = [1,2].p2collect_concat{ 'test' }
  raise "#{__method__} error" if r != [1, 'test', 2, 'test']
  p "#{__method__} passed"
end

def test_p2count
  r = [1,2,2].p2count{ |e| e == 2 }
  raise "#{__method__} error" if r != 2
  r = [1,2,2].p2count(2)
  raise "#{__method__} error" if r != 2
  r = [1,2,2].p2count
  raise "#{__method__} error" if r != 3
  p "#{__method__} passed"
end

def test_p2cycle
  r = [1,2,3].p2cycle(2){ |e| e+1 }
  raise "#{__method__} error" if r != [1,2,3,2,3,4]
  p "#{__method__} passed"
end

def test_p2detect
  r = [1,2,3].p2detect{ |e| e==2 }
  raise "#{__method__} error" if r != 2
  r = [1,2,3].p2detect(5+5){ |e| e==4 }
  raise "#{__method__} error" if r != 10
  r = [1,2,3].p2detect(nil){ |e| e==4 }
  raise "#{__method__} error" if r != nil
  p "#{__method__} passed"
end

def test_p2drop
  r = [1,2,3].p2drop(0)
  raise "#{__method__} error" if r != [1,2,3]
  r = [1,2,3].p2drop(2)
  raise "#{__method__} error" if r != [3]
  p "#{__method__} passed"
end

def test_p2drop_while
  r = [1,2,3].p2drop_while { |e| e<3 }
  raise "#{__method__} error" if r != [3]
  r = [1,2,3,2,1].p2drop_while { |e| e<2 }
  raise "#{__method__} error" if r != [2,3,2,1]
  p "#{__method__} passed"
end

def test_p2each
  s = 0
  [1,2,3].p2each{ |e| s += e }
  raise "#{__method__} error" if s != 6
  s = 0
  {"one"=>1,"two"=>2,"three"=>3}.p2each{ |f,e| s += e }
  raise "#{__method__} error" if s != 6
  p "#{__method__} passed"
end

def test_p2each_slice
  r = Array.new
  [1,2,3,4,5,6,7,8,9,10,11].p2each_slice(3) { |e| r << e }
  raise "#{__method__} error" if r != [[1,2,3],[4,5,6],[7,8,9],[10,11]]
  p "#{__method__} passed"
end

def test_p2each_with_index
  s = Array.new
  r = Array.new
  [2,3,4].each_with_index{ |e,i| s << e; r << i }
  raise "#{__method__} error" if (s != [2,3,4] || r != [0,1,2])
  p "#{__method__} passed"
end

def test_p2entries
  r = [1,2,3].p2entries
  raise "#{__method__} error" if r != [1,2,3]
  p "#{__method__} passed"
end

def test_p2find
  r = [1,2,3].p2find{ |e| e==2 }
  raise "#{__method__} error" if r != 2
  r = [1,2,3].p2find(5+5){ |e| e==4 }
  raise "#{__method__} error" if r != 10
  r = [1,2,3].p2find(nil){ |e| e==4 }
  raise "#{__method__} error" if r != nil
  p "#{__method__} passed"
end

def test_p2find_all
  r = [1,2,3].p2find_all { |e| e==2 }
  raise "#{__method__} error" if r != [2]
  r = [1,2,3,4,5,6].p2find_all { |e| e%2 == 0 }
  raise "#{__method__} error" if r != [2,4,6]
  r = [1,2,3].p2find_all { |e| e==4 }
  raise "#{__method__} error" if r != []
  p "#{__method__} passed"
end

def test_p2find_index
  r = [1,2,3].p2find_index { |e| e==2 }
  raise "#{__method__} error" if r != 1
  r = [1,2,3,4,5,6].p2find_index { |e| e == 7 }
  raise "#{__method__} error" if r != nil
  p "#{__method__} passed"
end

def test_p2first
  r = [1,2,3,4].p2first(2)
  raise "#{__method__} error" if r != [1,2]
  r = [1,2,3,4].p2first(5)
  raise "#{__method__} error" if r != [1,2,3,4]
  p "#{__method__} passed"
end

def test_p2group_by
  r = [1,2,3,4].p2group_by { |e| e%2 }
  raise "#{__method__} error" if r != {0=>[2,4], 1=>[1,3]}
  r = [1,2,3,4].p2group_by { |e| e }
  raise "#{__method__} error" if r != {1=>[1], 2=>[2], 3=>[3], 4=>[4]}
  p "#{__method__} passed"
end

def test_p2inject
  r = [1,2,3].p2inject(0){ |s, e| s + e }
  raise "#{__method__} error" if r != 6
  p "#{__method__} passed"
end

def test_p2minmax
  r = [1,2,3].p2minmax { |e| e+1 }
  raise "#{__method__} error" if r != [1,3]
  r = ['1','11','111'].p2minmax { |e| e.length }
  raise "#{__method__} error" if r != ['1','111']
  p "#{__method__} passed"
end

def test_p2minmax_by
  r = [1,2,3].p2minmax_by { |e| e+1 }
  raise "#{__method__} error" if r != [2,4]
  r = ['1','11','111'].p2minmax_by { |e| e.length }
  raise "#{__method__} error" if r != [1,3]
  p "#{__method__} passed"
end

def test_p2partition
  r = [1,2,3,4].p2partition { |e| e>2 }
  raise "#{__method__} error" if r != [[3,4],[1,2]]
  r = [1,2,3,4].p2partition { |e| e%2==0 }
  raise "#{__method__} error" if r != [[2,4],[1,3]]
  p "#{__method__} passed"
end

def test_p2reject
  r = [1,2,2].p2reject{ |e| e == 2 }
  raise "#{__method__} error" if r != [1]
  p "#{__method__} passed"
end

def test_p2take
  r = [1,2,3,4].p2take(2)
  raise "#{__method__} error" if r != [1,2]
  r = [1,2,3,4].p2take(5)
  raise "#{__method__} error" if r != [1,2,3,4]
  p "#{__method__} passed"
end

def test_p2take_while
  r = [1,2,3,4].p2take_while { |e| e < 3 }
  raise "#{__method__} error" if r != [1,2]
  r = [1,2,3,4].p2take_while { |e| e<0 }
  raise "#{__method__} error" if r != []
  r = [1,2,3,4].p2take_while { |e| e<5 }
  raise "#{__method__} error" if r != [1,2,3,4]
  p "#{__method__} passed"
end

def test_p2to_a
  r = (1..3).p2to_a
  raise "#{__method__} error" if r != [1,2,3]
  p "#{__method__} passed"
end

def test_p2to_h
  s = Array.new
  (1..3).p2to_a.p2each_with_index{ |e,i| s << [e, i] }
  r = s.p2to_h
  raise "#{__method__} error" if r != {1=>0, 2=>1, 3=>2}
  p "#{__method__} passed"
end
