class P2Tree
  @node
  @left
  @right

  def initialize( n = nil )
    if n != nil
      @node = n
      @left = P2Tree.new
      @right = P2Tree.new
    end
  end

  def insert( n )
    if @node == nil
      @node = n
      @left = P2Tree.new
      @right = P2Tree.new
    elsif n < @node
      @left.insert(n)
    else
      @right.insert(n)
    end
  end

  def p2each( &block )
    if @node != nil
      yield(@node)
    end
    if @left != nil
      @left.p2each(&block)
    end
    if @right != nil
      @right.p2each(&block)
    end
  end

  def p2each_with_level( &block )
    level = 0
    if @node != nil
      yield(level, @node)
    end
    level +=1
    if @left != nil
      @left.p2each_with_level_helper( level, &block )
    end
    if @right != nil
      @right.p2each_with_level_helper( level, &block )
    end
  end

  def p2each_with_level_helper( level, &block )
    if @node != nil
      yield(level, @node)
    end
    level +=1
    if @left != nil
      @left.p2each_with_level_helper( level, &block )
    end
    if @right != nil
      @right.p2each_with_level_helper( level, &block )
    end
  end

end
